#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *

from inverter2_gen.params import inverter2_layout_params
from pseudo_resistor_gen.params import pseudo_resistor_layout_params


@dataclass
class pseudo_resistor_ctrl_layout_params(LayoutParamsBase):
    """
    Parameter class for pseudo_resistor_ctrl_gen

    Args:
    ----
    inverter_params : inverter2_params
        Parameters for inverter2 sub-generators

    pseudo_resistor_params : pseudo_resistor_layout_params
        Parameters for pseudo_resistor sub-generators

    show_pins : bool
        True to create pin labels
    """

    inverter_params: inverter2_layout_params
    pseudo_resistor_params: pseudo_resistor_layout_params
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> pseudo_resistor_ctrl_layout_params:
        return pseudo_resistor_ctrl_layout_params(
            inverter_params=inverter2_layout_params.finfet_defaults(min_lch),
            pseudo_resistor_params=pseudo_resistor_layout_params.finfet_defaults(min_lch),
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> pseudo_resistor_ctrl_layout_params:
        return pseudo_resistor_ctrl_layout_params(
            inverter_params=inverter2_layout_params.planar_defaults(min_lch),
            pseudo_resistor_params=pseudo_resistor_layout_params.planar_defaults(min_lch),
            show_pins=True,
        )


@dataclass
class pseudo_resistor_ctrl_params(GeneratorParamsBase):
    layout_parameters: pseudo_resistor_ctrl_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> pseudo_resistor_ctrl_params:
        return pseudo_resistor_ctrl_params(
            layout_parameters=pseudo_resistor_ctrl_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
