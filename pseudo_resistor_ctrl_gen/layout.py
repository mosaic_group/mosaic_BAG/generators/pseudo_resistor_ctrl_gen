from typing import *

from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase

from sal.layout.manual_router import ManualRouter, ManualRouting, UPPER, LOWER, TrackConnection, TrackIntersectionPoint
from sal.layout.placement import PlacedInstances

from inverter2_gen.layout import inverter2
from pseudo_resistor_gen.layout import pseudo_resistor
from .params import pseudo_resistor_ctrl_layout_params


class layout(TemplateBase):
    """A Pseudo resistor with CNT including inverter and Pseudo resistor sub-blocks.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='pseudo_resistor_ctrl_layout_params parameters object',
        )

    def draw_layout(self):
        """Draw the layout."""

        # make copies of given dictionaries to avoid modifying external data.
        params: pseudo_resistor_ctrl_layout_params = self.params['params'].copy()
        inv_params = params.inverter_params
        ps_res_params = params.pseudo_resistor_params

        # disable pins in subcells
        inv_params.show_pins = False
        ps_res_params.show_pins = False

        # get layer IDs
        hm_layer = 4
        vm_layer = hm_layer + 1
        top_layer = vm_layer

        # create layout masters for subcells we will add later
        inv_master = self.new_template(temp_cls=inverter2, params=dict(params=inv_params))
        ps_res_master = self.new_template(temp_cls=pseudo_resistor, params=dict(params=ps_res_params))

        # add subcell instances (inv1 and inv2 in first row on bottom, PS_Res in second row on top)
        inv1_inst = self.add_instance(inv_master, 'inv1', loc=(0, 0), unit_mode=True)
        x1 = inv1_inst.bound_box.right_unit
        inv2_inst = self.add_instance(inv_master, 'inv2', loc=(x1, 0), unit_mode=True)
        x2 = inv2_inst.bound_box.top_unit
        x3 = ps_res_master.bound_box.right_unit
        ps_res_inst = self.add_instance(ps_res_master, 'PS_Res', loc=(x3, x2), unit_mode=True, orient='MY')

        placed_instances = PlacedInstances(template=self, instances=[inv1_inst, inv2_inst, ps_res_inst])
        placed_instances.set_size_from_bound_box(top_layer_id=top_layer)

        #
        # add wires, vias, pins
        #

        routing = ManualRouting(
            placed_instances=placed_instances.instances,

            connections_to_tracks=[
                # Net Name              /  Net Horizontal Wires      / Vertical track intersection point
                TrackConnection('ENB',  ['inv1.out', 'inv2.in'],     TrackIntersectionPoint('inv1.out',   UPPER, 1)),
                TrackConnection('OUT',  ['PS_Res.CNT', 'inv2.out'],  TrackIntersectionPoint('inv2.VDD',   UPPER)),
                TrackConnection('VDD',  ['PS_Res.VDD[0]',
                                         'PS_Res.VDD[1]',
                                         'inv1.VDD', 'inv2.VDD'],    TrackIntersectionPoint('PS_Res.VDD', LOWER)),
                TrackConnection('VSS',  ['inv1.VSS', 'inv2.VSS'],    TrackIntersectionPoint('inv1.VDD',   LOWER)),
            ],

            net_pins={
                'VDD': ['PS_Res.VDD[1]', 'PS_Res.VDD[0]', 'inv1.VDD', 'inv2.VDD'],
                'VSS': ['inv1.VSS', 'inv2.VSS'],
            },

            reexported_ports=[
                # Net name / Instance pin
                ('CNT',     'inv1.in'),
                ('I',       'PS_Res.I'),
                ('On',       'PS_Res.On'),
                ('Op',       'PS_Res.Op'),
            ]
        )

        router = ManualRouter(template=self, routing=routing, show_pins=params.show_pins)
        router.route()

        # compute schematic parameters.
        self._sch_params = dict(
            inv_params=inv_master.sch_params,
            ps_res_params=ps_res_master.sch_params,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class pseudo_resistor_ctrl(layout):
    """
    Class to be used as template in higher level layouts
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
