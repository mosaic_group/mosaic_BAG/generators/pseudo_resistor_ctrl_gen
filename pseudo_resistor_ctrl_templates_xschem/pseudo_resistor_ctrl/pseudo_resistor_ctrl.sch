v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 210 -80 210 -60 {
lab=VSS}
N 290 -130 310 -130 {
lab=On}
N 290 -110 310 -110 {
lab=Op}
N 170 -120 190 -120 {
lab=I}
N 210 -340 210 -330 {
lab=VDD}
N 320 -340 320 -330 {
lab=VDD}
N 350 -310 360 -310 {
lab=EN}
N 320 -290 320 -270 {
lab=VSS}
N 210 -290 210 -270 {
lab=VSS}
N 160 -310 180 -310 {
lab=CNT}
N 240 -310 290 -310 {
lab=ENB}
N 240 -170 240 -160 {}
N 210 -170 210 -160 {}
C {devices/iopin.sym} 60 -320 2 0 {name=p1 lab=CNT}
C {devices/iopin.sym} 60 -300 2 0 {name=p2 lab=I}
C {devices/iopin.sym} 60 -280 2 0 {name=p3 lab=On}
C {devices/iopin.sym} 60 -260 2 0 {name=p4 lab=Op}
C {devices/iopin.sym} 60 -210 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 60 -190 2 0 {name=p6 lab=VSS}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 190 -290 0 0 {name=inv1
}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 300 -290 0 0 {name=inv2
}
C {devices/lab_pin.sym} 260 -310 3 0 {name=l1 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 360 -310 2 0 {name=l2 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 210 -340 1 0 {name=l3 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 160 -310 0 0 {name=l4 sig_type=std_logic lab=CNT}
C {devices/lab_pin.sym} 210 -270 3 0 {name=l5 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 320 -270 3 0 {name=l6 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 320 -340 1 0 {name=l7 sig_type=std_logic lab=VDD}
C {pseudo_resistor_gen/pseudo_resistor_templates_xschem/pseudo_resistor/pseudo_resistor.sym} 170 -70 0 0 {name=Ps_Res
spiceprefix=X
}
C {devices/lab_pin.sym} 210 -170 1 0 {name=l8 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 210 -60 3 0 {name=l9 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 240 -170 1 0 {name=l10 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 310 -130 2 0 {name=l11 sig_type=std_logic lab=On}
C {devices/lab_pin.sym} 310 -110 2 0 {name=l12 sig_type=std_logic lab=Op}
C {devices/lab_pin.sym} 170 -120 0 0 {name=l13 sig_type=std_logic lab=I}
